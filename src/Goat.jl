# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

goat_df = DataFrame(CSV.File(datadir("InputData/myGoats.csv"); types = Dict(:Sex => Symbol)))

@agent Goat GridAgent{2} begin
    goat_id::Int64
    herder::Int64
    herd_id::Int64
    home::Int64
    age::Float16
    sex::Symbol
    energy::Float64
    hunger::Float16
    gave_birth::Bool
    max_energy::Int64
    meat_yield_kg::Float16
    milk_individual_day_l::Float16
    total_amount_of_meat_kg::Float16
    total_amount_of_milk_l::Float16
    days_since_last_birth::Float16
    gestation_period::Float16
    pregnant::Bool
    lactation_days::Float16
    simulated_days::Float16
    milking_simulated_days::Float16
    milked::Bool
end

function agent_step!(goat::Goat, model) 

    goat.simulated_days += 0.25

    all_agents = collect(allagents(model))

    all_herders = filter!(x -> isa(x, Herder), all_agents)

    herder = filter(herder -> herder.herd == goat.herd_id, all_herders)[1]

    if model.tick == 0
        rand_death = rand(model.rng, 1:10000)

        if rand_death == 2
            kill_agent!(goat, model)
        end
    end

    if model.tick == 1
        move_to_herder(goat, model)
    end

    if model.tick == 2

        if  model.biomass[goat.pos...] > 100

            graze!(goat, model)

        else
            rand_dist_walking = rand(model.rng, 3:4)

            for n in 1:rand_dist_walking
                walk!(goat, rand, model)
            end
            
            if  model.biomass[goat.pos...] > 100

                graze!(goat, model)
            else
                goat.energy -= 1
            end
        end       
    end

    if model.tick == 3
        move_home!(goat, model)
    end

    # fertility, pregnancy, gestation and birth giving according to Dahl and Hjort

    if goat.sex === :female

        if herder.number_goats < herder.max_number_goats * 0.9 && goat.lactation_days == 0 && goat.energy > goat.max_energy * 0.75 && goat.age > 1.553424657534247 + (rand(model.rng, 0:12) * 0.4166666666666666666666666666667)  / 365 - (rand(model.rng, 0:12) * 0.4166666666666666666666666666667)  / 365 && goat.days_since_last_birth > 20 + rand(model.rng, 0:20)
        
            goat.days_since_last_birth = 0
        
            goat.pregnant = true
        end

        if goat.pregnant == true
            goat.gestation_period += 0.25
        end

        abortion_probability = rand(model.rng, 0:10) # According to https://doi.org/10.1016/S0921-4488(02)00094-9

        if herder.number_goats < herder.max_number_goats * 0.9 && goat.gestation_period > 150 + rand(model.rng, 0:20) - rand(model.rng, 0:20) && model.month >= 3 && model.month <= 9 && abortion_probability != 10 ## successful 

            number_birth = 1 + rand(model.rng, 0:2)

            for n in 1:number_birth
                spawn_goat!(goat, model)
            end

            goat.gave_birth = true
            goat.pregnant = false
            goat.gestation_period = 0
    
        elseif goat.gestation_period > 150 + rand(model.rng, 0:20) - rand(model.rng, 0:20) && model.month >= 3 && model.month <= 9 && abortion_probability == 10  ## abortion
        
            goat.gave_birth = false
            goat.pregnant = false
            goat.gestation_period = 0
        end
    
        if goat.gave_birth == true || goat.pregnant == false
            goat.days_since_last_birth += 0.25
        end 

        if goat.gave_birth == true && goat.pregnant == false
            goat.lactation_days += 0.25
            goat.milking_simulated_days += 0.25
            goat.milk_individual_day_l = 0.19
        end

        if goat.lactation_days > 120 + rand(model.rng, 0:10)
            goat.gave_birth = false
            goat.lactation_days = 0
        end

        if goat.gestation_period > 180 + rand(model.rng, 0:10) - rand(model.rng, 0:10)
            goat.gestation_period = 0
            goat.pregnant = false
        end
    end
    
    # let the agents age and die because of age

    goat.age += (0.0027397272 / 4)

    if goat.age >= 4 + rand(model.rng, 0:0.01:1) #&& rand_death_goat == 1
        kill_agent!(goat, model)
    end

    # let the agents die due to 0 energy

    if goat.energy <= 0
        kill_agent!(goat, model)
    end

    # milk production

    if goat.gave_birth == true && goat.milk_individual_day_l > 0
        goat.total_amount_of_milk_l = ((goat.milk_individual_day_l + (goat.energy / 100 )) * goat.milking_simulated_days)
    end

    # meat production

    if goat.age >= 2 && goat.sex == :male
        goat.max_energy = 30 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
        goat.meat_yield_kg = 18
    elseif  goat.age >= 2 && goat.sex == :female
        goat.max_energy = 30 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
        goat.meat_yield_kg = 14.4
    end

    goat.total_amount_of_meat_kg = (goat.meat_yield_kg + (goat.energy / 10))
end

## move to herder

function move_to_herder(agent, model)

    all_agents = collect(allagents(model))

    all_herders = filter!(x -> isa(x, Herder), all_agents)

    own_herder = filter(herder -> herder.herd == agent.herd_id, all_herders)

    own_herder_df = DataFrame(own_herder)
    
    move_agent!(agent, own_herder_df.pos[1], model)
end


# reduce biomass

function graze!(agent, model)

    model.times_grazed[agent.pos...] += 1

    model.biomass[agent.pos...] -= agent.hunger
        
    if agent.energy < agent.max_energy
        agent.energy += 1
    end
end

# spawn new goats

function spawn_goat!(agent, model)
    id = nextid(model)
    goat_id = id
    herder = agent.herder
    herd_id = agent.herd_id
    home = agent.home
    age = 0
    
    rand_sex = rand(model.rng, 0:1)

    if rand_sex == 0
        sex = :female
    else
        sex = :male
    end
    
    energy = agent.energy / 2
    hunger = 6 + rand(model.rng, 0:2)
    gave_birth = false
    max_energy = 20 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
    meat_yield_kg = 9
    milk_individual_day_l = 0.0
    total_amount_of_meat_kg = (meat_yield_kg + (energy / 10)) 
    total_amount_of_milk_l = 0.0
    days_since_last_birth = 0.0
    gestation_period = 0.0
    pregnant = false
    lactation_days = 0.0
    simulated_days = 0.0
    milking_simulated_days = 0.0
    milked = false
    goat = Goat(id, (0, 0), goat_id, herder, herd_id, home, age, sex, energy, hunger, gave_birth, max_energy, meat_yield_kg, milk_individual_day_l, total_amount_of_meat_kg, total_amount_of_milk_l, days_since_last_birth, gestation_period, pregnant, lactation_days, simulated_days, milking_simulated_days, milked)
    add_agent!(goat, agent.pos, model)
end
