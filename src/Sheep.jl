# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

sheep_df = DataFrame(CSV.File(datadir("InputData/mySheep.csv"); types = Dict(:Sex => Symbol)))

@agent Sheep GridAgent{2} begin
    sheep_id::Int64
    herder::Int64
    herd_id::Int64
    home::Int64
    age::Float16
    sex::Symbol
    energy::Float64
    hunger::Float16
    gave_birth::Bool
    max_energy::Int64
    meat_yield_kg::Float16
    milk_individual_day_l::Float16
    total_amount_of_meat_kg::Float16
    total_amount_of_milk_l::Float16
    days_since_last_birth::Float16
    gestation_period::Float16
    pregnant::Bool
    lactation_days::Float16
    simulated_days::Float16
    milking_simulated_days::Float16
    milked::Bool
end

function agent_step!(sheep::Sheep, model) 

    sheep.simulated_days += 0.25

    all_agents = collect(allagents(model))

    all_herders = filter!(x -> isa(x, Herder), all_agents)

    herder = filter(herder -> herder.herd == sheep.herd_id, all_herders)[1]

    if model.tick == 0
        rand_death = rand(model.rng, 1:10000)

        if rand_death == 2
            kill_agent!(sheep, model)
        end
    end

    if model.tick == 1
        move_to_herder(sheep, model)
    end

    if model.tick == 2

        if  model.biomass[sheep.pos...] > 100
            graze!(sheep, model)
        else
            rand_dist_walking = rand(model.rng, 1:2)

            for n in 1:rand_dist_walking
                walk!(sheep, rand, model)
            end
            
            if  model.biomass[sheep.pos...] > 100
                graze!(sheep, model)
            else
                sheep.energy -= 1
            end
        end  
    end

    if model.tick == 3
        move_home!(sheep, model)
    end

    # fertility, pregnancy, gestation and birth giving according to Dahl and Hjort

    if sheep.sex === :female
        if herder.number_sheep < herder.max_number_sheep * 0.8 && sheep.lactation_days == 0 && sheep.energy > sheep.max_energy * 0.75 && sheep.age > 1.27 + (rand(model.rng, 0:90) * 0.4166666666666666666666666666667) / 365 - (rand(model.rng, 0:90) * 0.4166666666666666666666666666667) / 365  && sheep.days_since_last_birth > 40 + rand(model.rng, 15:19)
        
            sheep.pregnant = true
        end

        if sheep.pregnant == true
            sheep.gestation_period += 0.25
        end

        abortion_probability = rand(model.rng, 0:10) # According to https://doi.org/10.1016/S0921-4488(02)00094-9

        if herder.number_sheep < herder.max_number_sheep * 0.8 && sheep.gestation_period > 140 + rand(model.rng, 0:21) && model.month >= 3 && model.month <= 9 && abortion_probability != 10 ## successful carriage

            number_birth = 1 + rand(model.rng, 0:1)

            for n in 1:number_birth
                spawn_sheep!(sheep, model)
            end

            sheep.gave_birth = true
            sheep.pregnant = false
            sheep.gestation_period = 0
    
        elseif sheep.gestation_period > 140 + rand(model.rng, 0:10) - rand(model.rng, 0:10) && model.month >= 3 && model.month <= 9 && abortion_probability == 10  ## abortion
        
            sheep.gave_birth = false
            sheep.pregnant = false
            sheep.gestation_period = 0    
        end

        if sheep.sex == :female && sheep.gave_birth == true || sheep.sex == :female && sheep.pregnant == false
            sheep.days_since_last_birth += 0.25
        end 

        if sheep.gave_birth == true && sheep.pregnant == false
            sheep.lactation_days += 0.25
            sheep.milking_simulated_days += 0.25
            sheep.milk_individual_day_l = 0.14
        end

        if sheep.lactation_days >= 90 + rand(model.rng, 0:10)
            sheep.gave_birth = false
            sheep.lactation_days = 0
        end

        if sheep.gestation_period > 170  + rand(model.rng, 0:10) - rand(model.rng, 0:10) # miscarriage

            sheep.gestation_period = 0
            sheep.gave_birth = false
            sheep.pregnant = false

        end
    end
    
    # let the agents age and die because of age

    sheep.age += (0.0027397272 / 4)

    if sheep.age >= 4 + rand(model.rng, 0:0.01:1) 
        kill_agent!(sheep, model)
    end

    # let the agents die due to 0 energy

    if sheep.energy <= 0
        kill_agent!(sheep, model)
    end

    # milk production

    if sheep.gave_birth == true && sheep.milk_individual_day_l > 0
        sheep.total_amount_of_milk_l = ((sheep.milk_individual_day_l + (sheep.energy / 100 )) * sheep.milking_simulated_days)
    end

    # meat production

    if sheep.age >= 2 && sheep.sex == "male"
        sheep.max_energy = 30 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
        sheep.meat_yield_kg = 18
    elseif sheep.age >= 2 && sheep.sex == :female
        sheep.max_energy = 30 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
        sheep.meat_yield_kg = 14.4
    end
    
    sheep.total_amount_of_meat_kg = (sheep.meat_yield_kg + (sheep.energy / 10))
end

## move to herder

function move_to_herder(agent, model)

    all_agents = collect(allagents(model))

    all_herders = filter!(x -> isa(x, Herder), all_agents)

    own_herder = filter(herder -> herder.herd == agent.herd_id, all_herders)

    own_herder_df = DataFrame(own_herder)
    
    move_agent!(agent, own_herder_df.pos[1], model)
end

# reduce biomass

function graze!(agent, model)

    model.times_grazed[agent.pos...] += 1
            
    model.biomass[agent.pos...] -= agent.hunger
        
    if agent.energy < agent.max_energy
        agent.energy += 1
    end
end

# spawn new sheep

function spawn_sheep!(agent, model)
    id = nextid(model)
    sheep_id = id
    herder = agent.herder
    herd_id = agent.herd_id
    home = agent.home
    age = 0
    
    rand_sex = rand(model.rng, 0:1)

    if rand_sex == 0
        sex = :female
    else
        sex = :male
    end
    
    energy = agent.energy / 2
    hunger = 6 + rand(model.rng, 0:2)
    gave_birth = false
    max_energy = 20 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
    meat_yield_kg = 9
    milk_individual_day_l = 0.0
    total_amount_of_meat_kg = (meat_yield_kg + (energy / 10)) 
    total_amount_of_milk_l = 0.0
    days_since_last_birth = 0.0
    gestation_period = 0.0
    pregnant = false
    lactation_days = 0.0
    simulated_days = 0.0
    milking_simulated_days = 0.0
    milked = false
    sheep = Sheep(id, (0, 0), sheep_id, herder, herd_id, home, age, sex, energy, hunger, gave_birth, max_energy, meat_yield_kg, milk_individual_day_l, total_amount_of_meat_kg, total_amount_of_milk_l, days_since_last_birth, gestation_period, pregnant, lactation_days, simulated_days, milking_simulated_days, milked)
    add_agent!(sheep, agent.pos, model)
end
