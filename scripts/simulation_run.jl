# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

########################  
### Simulation setup ###
########################

    using DrWatson, Random

    #Random.seed!(2021) # set a global seed if you want your results to be completely reproducible

    @quickactivate "AgropastoralistLandUse"

    include(srcdir("AgropastoralistLandUse.jl"))

    model = initialize_model(;
        n_herders = 3,
        n_goats = 300,
        n_sheep = 300, 
        n_settlements = 1,
        n_population = 100,
        dims = (215, 124),
        tick = 0,
        day = 0.0,
        model_step = 0,
    )
        
        n_ticks = 73000 # define number of ticks; 1 tick is equal to 6 hours; 1 year = 1460 ticks

##########################
### Run the simulation ###
##########################

    @time Agents.step!(model, agent_step!, model_step!, n_ticks) 

##########################  
### Export the results ###
##########################

    safesave(datadir("sims/herder/herder_data_df.csv"), herder_data_df)

    safesave(datadir("sims/settlement/settlement_data_df.csv"), settlement_data_df)

    safesave(datadir("sims/population/population_data_df.csv"), population_data_df)
   
    raster = GeoArray(model.properties.times_grazed)

    bounds = bbox(biomass_matrix)

    bbox!(raster, bounds)

    epsg!(raster, 4326)

    i = rand(model.rng, 1:10000) # to avoid overwriting, a random number is generated and added to the raster name

    GeoArrays.write!("data/sims/grazing_pressure/times_grazed$(i).tif", raster)
